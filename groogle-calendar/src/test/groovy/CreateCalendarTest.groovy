import com.google.api.client.util.DateTime
import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.calendar.model.Calendar
import com.google.api.services.calendar.model.EventDateTime
import com.puravida.groogle.CreateEvent
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.CalendarScript
import com.puravida.groogle.WithCalendar
import com.puravida.groogle.WithEvent
CalendarScript.instance.with {
    //tag::login[]
    login {
        applicationName 'groogle-example'
        withScopes CalendarScopes.CALENDAR
        usingCredentials '/client_secret.json'
        asService false
    }
    //end::login[]

    String groogleCalendarId = "groovy.groogle@gmail.com"
//tag::createCalendar[]
    createEvent groogleCalendarId, { CreateEvent createEvent ->
        createEvent.event.summary = "quedada"
        allDay new Date().format('yyyy-MM-dd')
    }

    createEvent groogleCalendarId, { CreateEvent createEvent ->
        createEvent.event.summary = "borrachera"
        createEvent.attendee 'jagedn@gmail.com'
        between new Date() - 1, new Date()
        reminder 'email', 10
    }

//end::createCalendar[]
}