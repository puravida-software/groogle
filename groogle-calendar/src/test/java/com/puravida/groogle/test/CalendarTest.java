package com.puravida.groogle.test;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.calendar.CalendarScopes;
import com.puravida.groogle.*;
import org.junit.Before;
import org.junit.Test;
import com.google.api.services.calendar.model.Calendar;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;


public class CalendarTest {

    @Before
    public void doLogin(){
        //tag::login[]
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-example");
        groogleScript.login(clientSecret, Arrays.asList(CalendarScopes.CALENDAR));

        CalendarScript.instance.setGroogleScript(groogleScript);
        //end::login[]
    }

    @Test
    public void allCalendars() throws IOException{
        //tag::list[]
        for( CalendarBean c : CalendarScript.instance.allCalendars()){
            System.out.println(c.getId()+" "+c.getSummary()+" primary ?"+c.isPrimarry());
        };
        //end::list[]
    }

    @Test
    public void removeCalendar() throws IOException{
        //tag::removeCalendar[]
        CalendarScript.instance.removeCalendarFromUserList("este id no existe");
        //end::removeCalendar[]
    }

    @Test
    public void withCalendar() throws IOException{
        //tag::withCalendar[]
        String groogleCalendarId="groovy.groogle@gmail.com";
        CalendarScript.instance.withCalendar(groogleCalendarId, (WithCalendar withCalendar)->{
            withCalendar.batchSize(20);
        });
        //end::withCalendar[]
    }
}
