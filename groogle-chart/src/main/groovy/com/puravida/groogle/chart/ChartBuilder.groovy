package com.puravida.groogle.chart

class ChartBuilder {


    static Chart buildChart(int w, int h, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=ChartSpec)Closure closure){
        ChartSpec chartSpec = new ChartSpec(width: w, height: h)
        def code = closure.rehydrate(chartSpec,chartSpec,chartSpec)
        code.resolveStrategy = Closure.DELEGATE_ONLY
        code()
        new Chart(chartSpec)
    }

    static Chart buildChart(int w, int h, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=ChartSpec)String spec) {
        Closure closure = new GroovyShell().evaluate("{ -> $spec}")
        buildChart w, h, closure
    }
}
