package com.puravida.groogle.chart

import com.puravida.groogle.chart.core.BaseChartSpec
import com.puravida.groogle.chart.core.ValuesTrait

class Pie3DChartSpec extends BaseChartSpec implements ValuesTrait{

    String getTYPE(){
        'p3'
    }

}
