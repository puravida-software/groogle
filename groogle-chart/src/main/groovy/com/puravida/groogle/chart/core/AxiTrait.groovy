package com.puravida.groogle.chart.core

trait AxiTrait {

    private List<XYSerieSpec> xySerieSpec = []

    AxiTrait fixedValues(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=XYSerieSpec)Closure closure){
        XYSerieSpec spec = new XYSerieSpec()
        def code = closure.rehydrate(spec, spec,spec)
        code.resolveStrategy = Closure.DELEGATE_ONLY
        code()
        xySerieSpec.add spec
        this
    }

    String getValuesAsString(){
        String ret = ""

        ret = "chd=t:"
        ret += xySerieSpec.collect{ XYSerieSpec entry->
            String str = entry._valuesX.join(',')
            str += '|'
            str += entry._valuesY.join(',')
            str
        }.join('|')

        if( xySerieSpec.find{ it._labels.size() } ) {
            ret += '&chl='+xySerieSpec.collect {it._labels.join('|')}.join('|')
        }
        ret
    }

    Map<String,String>getValuesAsMap(){
        Map<String,String> ret = [:]

        if( xySerieSpec.size() ){
            String chd="t:"
            chd+= xySerieSpec.collect{ XYSerieSpec entry->
                String str = entry._valuesX.join(',')
                str += '|'
                str += entry._valuesY.join(',')
                str
            }.join('|')
            ret += [chd: chd]
        }
        if( xySerieSpec.find{ it._labels.size() != 0}){
            String chl = xySerieSpec.collect {it._labels.join('|')}.join('|')
            ret += [chl: chl]
        }
        ret
    }


}
