package com.puravida.groogle.chart.core

class SerieSpec {

    protected SerieSpec(){
    }

    protected List _values = []

    SerieSpec values(double[] values){
        this._values.addAll values
        this
    }

    SerieSpec values(int[] values){
        this._values.addAll values
        this
    }

    protected List<String> _labels = []
    SerieSpec withLabels(String[] labels){
        this._labels.addAll labels
        this
    }


}
