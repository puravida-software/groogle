package com.puravida.groogle.chart.core

trait ValuesTrait {

    private List<SerieSpec> _series = []

    ValuesTrait fixedValues(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=SerieSpec)Closure closure){
        SerieSpec serieSpec = new SerieSpec()
        def code = closure.rehydrate(serieSpec ,serieSpec,serieSpec)
        code.resolveStrategy = Closure.DELEGATE_ONLY
        code()
        _series.add serieSpec
        this
    }

    String getValuesAsString(){
        String ret = ""
        if( _series.find{ it._values.size() != 0}){
            ret = "chd=t:"
            ret += _series.collect {it._values.join(',')}.join('|')
        }
        if( _series.find{ it._labels.size() != 0}){
            ret += '&chl='
            ret += _series.collect {it._labels.join('|')}.join('|')
        }
        ret
    }

    Map<String,String>getValuesAsMap(){
        Map<String,String> ret = [:]
        if( _series.find{ it._values.size() != 0}){
            String chd = "t:" +_series.collect {it._values.join(',')}.join('|')
            ret += [chd: chd]
        }
        if( _series.find{ it._labels.size() != 0}){
            String chl = _series.collect {it._labels.join('|')}.join('|')
            ret += [chl: chl]
        }
        ret
    }
}
