package com.puravida.groogle.chart.core

class XYSerieSpec {

    protected XYSerieSpec(){
    }

    protected List<Number> _valuesX = []

    XYSerieSpec forEach(double[] values){
        this._valuesX.addAll values
        this
    }

    XYSerieSpec forEach(int[] values){
        this._valuesX.addAll values
        this
    }

    protected List<Number> _valuesY = []

    XYSerieSpec assign(double[] values){
        this._valuesY.addAll values
        this
    }

    XYSerieSpec assign(int[] values){
        this._valuesY.addAll values
        this
    }

    protected List<String> _labels = []
    XYSerieSpec withLabels(String[] labels){
        this._labels.addAll labels
        this
    }

}
