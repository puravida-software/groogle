package com.puravida.groogle.chart

import spock.lang.Specification


class Pie3DChartTestSpec extends Specification{


    def "pieChart with 3 values"(){
        given:
        String fileName = "build/${specificationContext.currentSpec.name}_${specificationContext.currentIteration.name}.png"
        new File(fileName).mkdirs()
        new File(fileName).delete()

        and:
        ChartSpec chartSpec = new ChartSpec()
        Chart chart = new Chart(chartSpec)

        when:
        chartSpec.pie3DChart  {
            fixedValues {
                values 1,2,3
            }
        }

        then:
        chart.chartSpec.baseChartSpec
        chart.chartSpec.baseChartSpec.toString() == 'cht=p3&chd=t:1,2,3'
        chart.GET == 'https://chart.googleapis.com/chart?chs=500x200&cht=p3&chd=t:1,2,3'
        chart.imageByGet.length
        chart.imageByPost.length

        and:
        new File(fileName) << chart.bytes

        and:
        new File(fileName).exists()

    }

    def "pieChart as text #spec"(){
        given:
        String fileName = "build/${specificationContext.currentSpec.name}_${specificationContext.currentIteration.name}.png"
        new File(fileName).mkdirs()
        new File(fileName).delete()

        when:
        Chart chart = ChartBuilder.buildChart(500,200,spec)

        then:
        chart.chartSpec
        chart.GET == 'https://chart.googleapis.com/chart?chs=500x200&cht=p3&chd=t:1,2,3'
        chart.bytes.length

        and:
        new File(fileName) << chart.bytes

        and:
        new File(fileName).exists()


        where:
        spec = """pie3DChart{
            fixedValues {
                values 1,2,3   
            }
        }"""

    }

    def "pieChart as #closure"(){
        given:
        String fileName = "build/${specificationContext.currentSpec.name}_${specificationContext.currentIteration.name}.png"
        new File(fileName).mkdirs()
        new File(fileName).delete()

        when:
        Chart chart = ChartBuilder.buildChart(500,200,closure)

        then:
        chart.chartSpec
        chart.GET == 'https://chart.googleapis.com/chart?chs=500x200&cht=p3&chd=t:1,2,3'
        chart.bytes.length

        and:
        new File(fileName) << chart.bytes

        and:
        new File(fileName).exists()

        where:
        closure = {
            pie3DChart {
                fixedValues {
                    values 1,2,3
                }
            }
        }
    }

}
