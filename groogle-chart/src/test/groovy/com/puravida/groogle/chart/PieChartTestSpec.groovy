package com.puravida.groogle.chart

import spock.lang.Specification


class PieChartTestSpec extends Specification{


    def "pieChart with 3 values"(){
        given:
        ChartSpec chartSpec = new ChartSpec(width: 100,height: 100)
        Chart chart = new Chart(chartSpec)

        when:
        chartSpec.pieChart{
            fixedValues {
                values 1,2,3
            }
        }

        then:
        chart.chartSpec.baseChartSpec
        chart.chartSpec.baseChartSpec.toString() == 'cht=p&chd=t:1,2,3'
        chart.GET == 'https://chart.googleapis.com/chart?chs=100x100&cht=p&chd=t:1,2,3'
        chart.bytes.length
    }

    def "pieChart as text #spec"(){

        when:
        Chart chart = ChartBuilder.buildChart(100,100,spec)

        then:
        chart.chartSpec
        chart.GET == 'https://chart.googleapis.com/chart?chs=100x100&cht=p&chd=t:1,2,3'
        chart.bytes.length

        where:
        spec = """pieChart{
            fixedValues {
                values 1,2,3   
            }
        }"""

    }

    def "pieChart as #closure"(){

        when:
        Chart chart = ChartBuilder.buildChart(100,100,closure)

        then:
        chart.chartSpec
        chart.GET == 'https://chart.googleapis.com/chart?chs=100x100&cht=p&chd=t:1,2,3'
        chart.bytes.length

        where:
        closure = {
            pieChart {
                fixedValues {
                    values 1,2,3
                }
            }
        }

    }

    def "pieChart as #closure with labels "(){

        when:
        Chart chart = ChartBuilder.buildChart(100,100,closure)

        then:
        chart.chartSpec
        chart.GET == 'https://chart.googleapis.com/chart?chs=100x100&cht=p&chd=t:1,2,3&chl=A|B|C'
        chart.bytes.length

        where:
        closure = {
            pieChart {
                fixedValues {
                    values 1,2,3 withLabels 'A','B','C'
                }
            }
        }

    }

}
