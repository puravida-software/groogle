package com.puravida.groogle

import groovy.transform.CompileStatic

@CompileStatic
abstract class AbstractScript {

    protected GroogleScript groogleScript

    protected abstract initScript(GroogleScript groogleScript)

    AbstractScript login(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=LoginSpec)Closure closure){
        GroogleScript.instance.login(closure)
        this.setGroogleScript(GroogleScript.instance)
        this
    }

    void setGroogleScript(GroogleScript groogleScript){
        this.groogleScript=groogleScript
        initScript(groogleScript)
    }

    void evaluate( File txt, Object delegate=null ){
        evaluate( txt.text, delegate)
    }

    void evaluate( String txt, Object delegate=null ){
        String evaluate = """{ script-> script.with{ $txt } }"""
        Closure closure = new GroovyShell().evaluate(evaluate) as Closure
        Closure cl = closure.rehydrate(delegate ?: this, delegate ?: this, delegate ?: this)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }


}
