package com.puravida.groogle

class LoginSpec {

    private String applicationName
    LoginSpec applicationName(String name){
        applicationName  = name
        this
    }

    String getAllDriveScopes(){
        com.google.api.services.drive.DriveScopes.DRIVE
    }

    String getAllCalendarScopes(){
        com.google.api.services.calendar.CalendarScopes.CALENDAR
    }

    String getAllSheetsScopes(){
        com.google.api.services.sheets.v4.SheetsScopes.SPREADSHEETS
    }

    private List<String> scopes = []
    LoginSpec withScope(String scope){
        scopes=[scope]
        this
    }

    LoginSpec withScopes(List<String> scopes){
        withScopes(scopes as String[])
    }

    LoginSpec withScopes(String[] scopes){
        this.scopes = scopes
        this
    }

    private InputStream clientSecret

    LoginSpec usingCredentials(String fileName){
        File f = new File(fileName)
        if( f.exists()  ){
            usingCredentials(f)
        }else{
            InputStream resource = this.class.getResourceAsStream(fileName)
            if( resource ){
                usingCredentials(resource)
            }else{
                throw new RuntimeException("Credentials $fileName not found")
            }
        }
    }

    LoginSpec usingCredentials(File clientSecret){
        usingCredentials(clientSecret.newInputStream())
    }

    LoginSpec usingCredentials(InputStream clientSecret){
        this.clientSecret=clientSecret
        this
    }

    private boolean asService=false
    LoginSpec asService(boolean b=true){
        asService=b
        this
    }

    private GroogleScript instance
    LoginSpec(GroogleScript instance){
        this.instance = instance
    }

    protected execute(){
        instance.applicationName=applicationName
        if( asService )
            instance.loginService(clientSecret,scopes)
        else
            instance.login(clientSecret,scopes)
    }

}
