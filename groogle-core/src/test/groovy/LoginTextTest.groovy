import com.puravida.groogle.GroogleScript

GroogleScript.instance.evaluate GroogleScript.instance,"""

    login{

        applicationName 'groogle-example'
    
        withScopes allDriveScopes, allCalendarScopes, allSheetsScopes
    
        usingCredentials( '/client_secret.json' )
    
        asService false
    }
"""