package com.puravida.groogle.test;

import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.puravida.groogle.GroogleScript;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.Assert.*;

public class LoginTest {

    @Test
    public void basicUnitTest() throws IOException{
//tag::test[]
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");

        GroogleScript groogleScript = GroogleScript.instance;
        groogleScript.setApplicationName("groogle-test");

        groogleScript.login(clientSecret, Arrays.asList(
                CalendarScopes.CALENDAR,
                DriveScopes.DRIVE,
                SheetsScopes.SPREADSHEETS
        ));
//end::test[]
    }

    @Test
    public void serviceUnitTest() throws IOException{
//tag::serviceLogin[]
        InputStream clientSecret = getClass().getResourceAsStream("/groogle-688bcfc07d1b.json");

        GroogleScript groogleScript = GroogleScript.instance;
        groogleScript.setApplicationName("groogle-test");

        groogleScript.loginService(clientSecret, Arrays.asList(
                CalendarScopes.CALENDAR,
                DriveScopes.DRIVE,
                SheetsScopes.SPREADSHEETS
        ));
//end::serviceLogin[]
    }


}
