= Groogle
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:doctype: book
:toc: left
:toclevels: 4
:source-highlighter: coderay
:imagesdir: images
:icons: font
:setanchors:
:idprefix:
:idseparator: -
:toc-collapsable:
:multilanguage: es,gb
:multilanguage-toolbar: toc
:google-analytics-code: UA-687332-11
:theme: colony
:ensure-https:

Groovy + Google = Groogle

== Groogle Drive

La libreria GroogleDrive nos permite acceder y gestionar ficheros
de Google Drive usando Groovy Script

== Dependencia

GroogleDrive depende de GroogleCore en la que delega la lógica de autentificación
y autorización

=== Gradle

[source,groovy,subs="attributes"]
----
repositories {
    jcenter()
}

compile 'com.puravida.groogle:groogle-core:{groogle-version}'
compile 'com.puravida.groogle:groogle-drive:{groogle-version}'
----

=== Grape

[source,groovy,subs="attributes"]
----
@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '{groogle-version}')
@Grab(group = 'com.puravida.groogle', module = 'groogle-drive', version = '{groogle-version}')
----

== Identificación y permisos

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/AllFilesTest.groovy[tag=script]
----

.Java
[source,java]
----
include::{srcDir}/test/java/com/puravida/groogle/test/AllFileTest.java[tag=login]
----

== Listar carpetas

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/AllFoldersTest.groovy[tag=script]
----

.Java
[source,java]
----
include::{srcDir}/test/java/com/puravida/groogle/test/AllFoldersTest.java[tag=script]
----

== Descargar carpetas

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/DownloadFolderTest.groovy[tag=script]
----


== Listar documentos

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/AllFilesTest.groovy[tag=script]
----

.Java
[source,java]
----
include::{srcDir}/test/java/com/puravida/groogle/test/AllFileTest.java[tag=script]
----


== withFiles

=== Obtener atributos

Para facilitar la gestión de los documentos existentes en Drive haremos la lógica
dentro de la closure *withFiles* que nos ofrece el servicio. En dicha closure
podremos aplicar algunos filtros y parámetros para optimizar la consulta.

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/PrintWithFilesTest.groovy[tag=script]
----
<1> Limitar la busqueda al entorno 'domain' (otros posibles valores en https://developers.google.com/drive/v3/reference/files/list)
<2> Filtrar por documentos cuyo nombre exacto sea ...
<3> Filtrar por documentos cuyo nombre empieze por ...
<4> Recuperar documentos en grupos de N
<5> Para cada fichero ejecutar la closure
<6> Podemos acceder a los atributos de forma dinámica o explícita

.Java
[source,java]
----
include::{srcDir}/test/java/com/puravida/groogle/test/PrintWithFilesTest.java[tag=script]
----


=== Salvar atributos

Si deseamos modificar algún atributo del documento (titulo, nombre, etc) simplemente modificaremos dicho(s) atributo(s)
e invocaremos el método *save* sin parámetros

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/SaveWithFilesTest.groovy[tag=script]
----
<1> modificamos un metadato
<2> comprobamos que ha sido modificado

=== Actualizar contenido

Si deseamos modificar el contenido del documento (además de algún metadato) deberemos proporcionar un _InputStream_ con
el nuevo contenido.

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/SaveContentWithFilesTest.groovy[tag=script]
----
<1> modificamos el contenido
<2> comprobamos que ha sido modificado

== Subir fichero

Para facilitar la subida de documentos nuevos a Drive usaremos la closure *uploadFile* que nos ofrece el servicio.
Mediante dicha closure podremos especificar el contenido así como atributos a adjuntar:

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/UploadFileTest.groovy[tag=script]
----
<1> crear documento desde InputStream
<2> subir y convertir a Sheet automaticamente
<3> crear un documento desde un fichero local
<4> subir y convertir a Doc automaticamente


== File

=== Buscar un Fichero

.Groovy
[source,groovy]
----
DriveScript.instance.findFile(id)
----
=== removeFromGoogle

Mediante este método eliminamos el documento de nuestro Drive

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/RemoveTest.groovy[tag=script]
----
<1> Eliminamos el fichero que acabamos de crear
