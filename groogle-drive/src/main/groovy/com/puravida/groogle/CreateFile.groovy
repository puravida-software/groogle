package com.puravida.groogle

import com.google.api.client.http.InputStreamContent
import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File

class CreateFile {

    Drive service

    File fileMetadata = new File();

    InputStream content

    def methodMissing(String name, args) {
        def metaMeth = this.metaClass.getMetaMethod(name,args)
        if(metaMeth){
            return metaMeth.invoke(this,args)
        }
        this.fileMetadata."$name" = "$args"
    }

    void name(String name){
        this.fileMetadata.name=name
    }

    void saveAs(String mimeType){
        this.fileMetadata.mimeType=mimeType
    }

    String GoogleDoc = 'application/vnd.google-apps.document'
    String GoogleSheet = 'application/vnd.google-apps.spreadsheet'

    void content(java.io.File f){
        if( !fileMetadata.mimeType )
            fileMetadata.mimeType = URLConnection.fileNameMap.getContentTypeFor(f.name)
        if( !fileMetadata.name )
            fileMetadata."name" = f.name
        this.content = new FileInputStream(f)
    }

    void content(java.io.FileInputStream f){
        if( !fileMetadata.mimeType )
            fileMetadata.mimeType = URLConnection.fileNameMap.getContentTypeFor(f.name)
        if( !fileMetadata.name )
            fileMetadata."name" = f.name
        this.content = f
    }

    void content(java.io.InputStream f){
        this.content = f
    }

    void intoFolder(String id){
        fileMetadata.parents = [id]
    }

    void intoFolder(File folder){
        assert folder.mimeType == 'application/vnd.google-apps.folder'
        fileMetadata.parents = [folder.id]
    }


    protected File execute(){
        assert fileMetadata.get('name') != null, "Name required"
        assert fileMetadata.get('mimeType')!=null, "MimeType required"

        InputStreamContent inputStreamContent= new InputStreamContent(fileMetadata.mimeType,content)
        def request = service.files().create(fileMetadata,inputStreamContent)
        request.fields = 'id'
        if( fileMetadata.parents )
            request.fields += ', parents'
        request.execute()
    }
}
