import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript


DriveScript.instance.with {
    login {
        applicationName 'groogle-example'
        withScopes DriveScopes.DRIVE
        usingCredentials '/client_secret.json'
        asService false
    }
    def f1 = uploadFile {
        name 'Codemotion2018.txt'

        mimeType 'text/plain'

        saveAs GoogleDoc

        content new ByteArrayInputStream("""
Hola Codemotion 2018 

son las ${new Date()}

""".bytes)


    }

    println f1.id
}