import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {
    //tag::login[]
    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
    //end::login[]

    withFiles {

        //findIn 'domain'     //<1>

        //nameEq 'Ejemplo'   //<2>

        nameStartWith 'Ejem'   //<3>

        batchSize 2         //<4>

        eachFile { arg ->  //<5>
            println "$arg.id $file.name"
            assert id == file.id    //<6>
            assert name == file.name
        }
    }
}
//end::script[]