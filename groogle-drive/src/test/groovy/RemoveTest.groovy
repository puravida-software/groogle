import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {
    //tag::login[]
    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
    //end::login[]

    def f3 = uploadFile{
        content new File('test.docx')
        saveAs GoogleDoc
    }
    println "Now we'll remove $f3.id"
    withFile f3.id,{
        removeFromDrive      //<1>
    }
}
//end::script[]
