package com.puravida.groogle.test;

import com.google.api.services.drive.model.File;
import com.puravida.groogle.*;
import groovy.lang.Closure;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


public class PrintWithFilesTest {

    @Before
    public void doLogin(){
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-example");
        groogleScript.login(clientSecret, Arrays.asList("https://www.googleapis.com/auth/drive"));

        DriveScript.instance.setGroogleScript(groogleScript);
    }

    @Test
    public void withFilesConsumers()throws IOException{
        //tag::script[]
        DriveScript.instance.withFiles(
                (WithFiles withFile)->{
                    withFile.nameStartWith("test");
                    withFile.orderBy("name");
                    withFile.batchSize(20);
                },
                (File file)->{
                    System.out.println(file.getId()+" "+file.getName()+ " "+file.getParents());
                    System.out.println("Folder "+DriveScript.instance.findFile(file.getParents().get(0)));
                }
        );
        //end::script[]
    }


}
