package com.puravida.groogle

import com.google.maps.model.LatLng

trait IndicationTrait {

    String address
    IndicationTrait address(String address){
        this.address=address
        this
    }

    Double latitud
    IndicationTrait latitud(Double latitud){
        this.latitud=latitud
        this
    }

    Double longitud
    IndicationTrait longitud(Double longitud){
        this.longitud=longitud
        this
    }

    IndicationTrait coordinates(Double latitud,Double longitud){
        this.latitud=latitud
        this.longitud=longitud
        this
    }

    String placeId
    IndicationTrait placeId(String placeId){
        this.placeId=placeId
        this
    }

}