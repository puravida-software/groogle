package com.puravida.groogle

import com.google.maps.model.DirectionsResult
import org.junit.Test

class DirectionsMapsTest {

    @Test
    void directionTest() {
        InputStream resource = this.class.getResourceAsStream('/apikey.txt')
        String apikeyText = resource.text

        MapsScript.instance.with {
            //tag::login[]
            config {
                apiKey apikeyText
            }
            //end::login[]

            //tag::directions[]
            driving{

                from"calle pescara, madrid"

                to 40.5316914, -3.891624

            }.routes.each{
                println it.summary
                it.legs.each{
                    println "\t $it.distance ($it.duration)"
                }
            }
            //end::directions[]

            //tag::others[]
            walking{

                from "calle pescara, madrid"

                to 40.5316914, -3.891624

                departure now

                wayPoint "plaza mayor, madrid"

                wayPoints 'plaza mayor, madrid', 'calle princesa, madrid'

                wayPoint 'palacio de la zarzuela'

            }.routes.each{
                println "$it.summary , coste : ${it.fare ?: 0.0}"
                it.legs.each{
                    println "\t $it.distance ($it.duration)"
                    println "\t\t $it.startAddress -> $it.endAddress"
                }
                println "$it.copyrights"
            }
            //end::others[]
        }

    }

}
