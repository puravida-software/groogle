package com.puravida.groogle

import com.google.maps.model.GeocodingResult
import com.puravida.groogle.MapsScript
import org.junit.Test

public class GeoMapsTest {

    @Test
    void geoTest(){
        InputStream resource = this.class.getResourceAsStream('/apikey.txt')
        String apikeyText = resource.text

        MapsScript.instance.with {
            //tag::login[]
            config {
                apiKey apikeyText
            }
            //end::login[]

            //tag::byAddress[]
            geoLocalization {
                address 'mi casa'
                eachResult {    //<1>
                    println formattedAddress
                }
            }
            //end::byAddress[]

            println "-".multiply(10)
            //tag::byLatLong[]
            geoLocalization {
                latitud = 40.0
                longitud = -3.0
                eachResult {
                    println formattedAddress
                }
            }
            //end::byLatLong[]

            println "-".multiply(10)
            //tag::placeId[]
            geoLocalization {
                placeId = 'ChIJd8BlQ2BZwokRAFUEcm_qrcA'
                reverseResult{
                    println formattedAddress
                }
            }
            //end::placeId[]

            println "-".multiply(10)
            //tag::reverseResult[]
            geoLocalization {
                latitud = 40.0
                longitud = -3.0
                reverseResult{  //<1>
                    println formattedAddress
                }
            }
            //end::reverseResult[]

            println "-".multiply(10)
            //tag::filter[]
            geoLocalization {
                latitud = 40.0
                longitud = -3.0
                usingIsoCode 'es' //<1>
                bounds 39.0, -4, 41.123, -2 //<2>
                region 'es' //<3>
                reverseResult{
                    println formattedAddress
                }
            }
            //end::filter[]

            println "-".multiply(10)
            //tag::result[]
            def results = geoLocalization {
                latitud = 40.0
                longitud = -3.0
                usingIsoCode 'es'
            }

            results.each{ GeocodingResult geocodingResult->

                println geocodingResult.formattedAddress

            }
            //end::result[]
        }
    }
}