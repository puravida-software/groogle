package com.puravida.groogle

import com.google.api.services.drive.model.File
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.Spreadsheet

import java.util.function.Consumer

@Singleton
class SheetScript extends AbstractScript{

    protected Sheets service

    DriveScript driveScript

    AbstractScript initScript(GroogleScript groogleScript){
        this.service = new Sheets.Builder(groogleScript.httpTransport, groogleScript.jsonFactory, groogleScript.credential)
                .setApplicationName(groogleScript.applicationName)
                .build()
        this
    }

    List<File> getSpreadSheets() throws IOException{
        listSpreadSheets
    }

    List<File>  getListSpreadSheets() throws IOException{
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        driveScript.allFiles('application/vnd.google-apps.spreadsheet')
    }

    String createSpreadSheet() throws IOException{
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        Spreadsheet spreadsheet = new Spreadsheet()
        service.spreadsheets().create(spreadsheet).execute().spreadsheetId
    }

    void withSpreadSheet(String spreadSheetId, Consumer<WithSpreadSheet> consumer){
        withSpreadSheet(spreadSheetId,{WithSpreadSheet withSpreadSheet->
            consumer.accept(withSpreadSheet)
        })
    }

    void withSpreadSheet( String spreadSheetId, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithSpreadSheet)Closure closure){
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        WithSpreadSheet spreadSheet = new WithSpreadSheet(spreadSheetId:spreadSheetId,service: service)
        Closure c = closure.rehydrate(spreadSheet,spreadSheet,spreadSheet)
        c.resolveStrategy=Closure.DELEGATE_ONLY
        c.call(spreadSheet)
        spreadSheet.execute()
    }
}