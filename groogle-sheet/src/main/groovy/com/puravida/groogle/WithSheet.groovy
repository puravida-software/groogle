package com.puravida.groogle

import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.BatchClearValuesRequest
import com.google.api.services.sheets.v4.model.ValueRange

import java.util.function.Consumer

class WithSheet  {

    protected Sheets service

    protected String spreadSheetId

    protected String sheetId

    def getProperty(String propertyName){
        def metaProp = this.metaClass.getMetaProperty(propertyName)
        if( metaProp )
            return metaProp.getProperty(this)
        def matcher = propertyName =~ /([A-Z][a-z]*)([0-9]+)?/
        if(!matcher.find())
            throw new MissingPropertyException(propertyName)
        readCell propertyName
    }

    void setProperty(String propertyName, Object newValue){
        def metaProp = this.metaClass.getMetaProperty(propertyName)
        if( metaProp ) {
            metaProp.setProperty(this, newValue)
            return
        }
        def matcher = propertyName =~ /([A-Z][a-z]*)([0-9]+)?/
        if(!matcher.find())
            throw new MissingPropertyException(propertyName)
        writeCell propertyName, newValue
    }

    List readCell(String rangeA){
        def list = readRows(rangeA, rangeA)
        list && list.size() ? list[0] : null
    }

    List<List> readRows(String rangeA, String rangeB){
        def request = service.spreadsheets().values().get(spreadSheetId,"$sheetId!$rangeA:${rangeB?:rangeA}")
        request.majorDimension = 'ROWS'
        request.valueRenderOption = 'FORMATTED_VALUE'
        request.execute().values
    }

    def readCols(String rangeA, String rangeB=null ){
        def request = service.spreadsheets().values().get(spreadSheetId,"$sheetId!$rangeA:${rangeB?:rangeA}")
        request.majorDimension = 'COLUMNS'
        request.valueRenderOption = 'FORMATTED_VALUE'
        request.execute().values
    }

    def writeCell(String rangeA, value ){
        ValueRange valueRange = new ValueRange(range:"$sheetId!$rangeA:$rangeA",
                majorDimension:'ROWS',
                values:[[value]])
        def request = service.spreadsheets().values().update(spreadSheetId,"$sheetId!$rangeA:$rangeA",valueRange)
        request.valueInputOption='USER_ENTERED'
        request.execute()
    }

    def writeInRange(String rangeA, String rangeB, Consumer<WriteInRange>writeInRangeConsumer){
        writeInRange(rangeA, rangeB, { WriteInRange writeInRange->
            writeInRangeConsumer.accept(writeInRange)
        })
    }

    def writeInRange(String rangeA, String rangeB, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WriteInRange)Closure closure){
        WriteInRange writeInRange = new WriteInRange(sheet: this)
        Closure c = closure.rehydrate(writeInRange,writeInRange,writeInRange)
        c.resolveStrategy = Closure.DELEGATE_FIRST
        c.call(writeInRange)

        ValueRange valueRange = new ValueRange(range:"$sheetId!$rangeA:$rangeB",
                majorDimension:'ROWS',
                values:writeInRange.rangeValues)
        def request = service.spreadsheets().values().update(spreadSheetId,"$sheetId!$rangeA:$rangeB",valueRange)
        request.valueInputOption='USER_ENTERED'
        request.execute()
    }

    def clearRange( String rangeA, String rangeB ){
        BatchClearValuesRequest request = new BatchClearValuesRequest(ranges:["$sheetId!$rangeA:$rangeB".toString()])
        service.spreadsheets().values().batchClear(spreadSheetId,request).execute()
    }

    def appendRow( Map rows ){
        rows.entrySet().each{
            appendRows( [it.value], it.key, it.key)
        }
    }

    def appendRow( Object[] values, String rangeA = "A", String rangeB = "A" ){
        appendRow( values as List, rangeA, rangeB)
    }

    def appendRow(List values, String rangeA="A", String rangeB="A" ){
        appendRows([values], rangeA, rangeB)
    }

    def appendRows(List<List> values, String rangeA="A", String rangeB="A"  ){
        ValueRange valueRange = new ValueRange()
        valueRange.values = values
        def request = service.spreadsheets().values().append(spreadSheetId,"$sheetId!$rangeA:$rangeB",valueRange)
        request.valueInputOption='USER_ENTERED'
        request.execute()
    }

    void execute(){

    }

}
