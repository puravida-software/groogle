package com.puravida.groogle

class WriteInRange {

    protected List rangeValues = []

    protected WithSheet sheet

    def addRow(Object...values){
        addRow( values as List)
    }

    def addRow(List values){
        rangeValues[rangeValues.size]=values
    }

    def setRow(int idx, List values){
        rangeValues[idx] = values
    }
}
