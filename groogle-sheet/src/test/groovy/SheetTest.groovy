import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.SheetScript

SheetScript.instance.with {
//tag::login[]
    login {
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE, SheetsScopes.SPREADSHEETS

        usingCredentials '/client_secret.json'

        asService false
    }
//end::login[]

    //tag::listSheets[]
    spreadSheets.each{
        println "$it.name ($it.id)"
    }
    //end::listSheets[]

    String spreadSheetId = spreadSheets.size() ? spreadSheets.first().id : null

    if (!spreadSheetId){
        println "No hay ninguna hoja de calculo con la que probar"
        return
    }
//tag::withSpreadSheet[]
    withSpreadSheet spreadSheetId, { spreadSheet ->

        //tag::sheets[]
        sheets.each {
            println "$it.properties.title"
        }
        //end::sheets[]

        String sheetId = sheets.first().properties.title

        //tag::clearRange[]
        println "borrar un rango de valores"
        withSheet sheetId, {
            clearRange 'A', 'AZ'
        }
        //end::clearRange[]

        //tag::write[]
        println "Escribir (ojo, hasta formulas)"
        withSheet sheetId, {    //<1>

            A2 = 'Hola' //<2>

            writeCell "A2", 'hola'   //<3>

            writeInRange 'A2', 'B4', {
                setRow 0, ['hola', null]  //<4>
                setRow 1, ['caracola', 'hola']
                addRow 'hola', '=A2'    //<5>
            }
        }
        //end::write[]

        //tag::append[]
        println "Append to the end"
        withSheet sheetId, { sheet ->   //<1>

            appendRow ('hola', 'caracola', new Date().time )  //<2>

            appendRows( [ //<3>
                         ['hola', 'caracola', new Date().time],
                         ['hola', 'caracola', new Date().time],
                         ['hola', 'caracola', new Date().time],
            ])

            appendRow 'A40':['hola', 'caracola', new Date().time],    //<4>
                    'A41':['=A40', '=B40', '=C40']
        }
        //end::append[]

        //tag::read[]
        println "Leer celda o rangos"
        withSheet sheetId, { sheet ->   //<1>

            def str = A2  //<2>
            println "A2 vale $str"

            def R2 = readCell("A2") //<3>
            println "A2 vale con readCell $R2"


            def A2B2 = readRows("A2", "B2") //<4>
            A2B2.each {
                println it
            }

            (1..10).each { idx ->
                println idx + " " + sheet."A$idx" //<5>
            }

            println "$A41 $B41 $C41"
        }
        //end::read[]

    }
    //end::withSpreadSheet[]
}