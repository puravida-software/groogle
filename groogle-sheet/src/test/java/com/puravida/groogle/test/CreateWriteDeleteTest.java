package com.puravida.groogle.test;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.sheets.v4.model.Sheet;
import com.puravida.groogle.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;


public class CreateWriteDeleteTest {

    @Before
    public void doLogin(){
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-example");
        groogleScript.login(clientSecret, Arrays.asList("https://www.googleapis.com/auth/drive"));

        SheetScript.instance.setGroogleScript(groogleScript);
    }

    @Test
    public void createDelete() throws IOException{
        String sheetId = SheetScript.instance.createSpreadSheet();

        try {
            DriveScript.instance.withFile(sheetId, (WithFile withFile)->{
                withFile.removeFromDrive();
            });

            DriveScript.instance.findFile(sheetId);
        }catch( Exception e){
            assert e instanceof GoogleJsonResponseException;
            GoogleJsonResponseException ge = (GoogleJsonResponseException)e;
            assert ge.getStatusCode() == 404;
        }
    }

    @Test
    public void createWriteDelete() throws IOException{

        //tag::createSheet[]
        String sheetId = SheetScript.instance.createSpreadSheet();
        //end::createSheet[]

        try {
            //tag::withSpreadSheet[]
            SheetScript.instance.withSpreadSheet(sheetId, (WithSpreadSheet withSpreadSheet)->{
                assert withSpreadSheet.getSheets().size() != 0;

                //tag::sheets[]
                for(Sheet sheet:withSpreadSheet.getSheets()){
                    sheet.getProperties().getTitle();
                }
                //end::sheets[]

                //tag::clearRange[]
                withSpreadSheet.withSheet(withSpreadSheet.getSheets().get(0).getProperties().getTitle(),(WithSheet withSheet)-> {
                            withSheet.clearRange("A", "AZ");
                });
                //end::clearRange[]

                //tag::java[]
                withSpreadSheet.withSheet(withSpreadSheet.getSheets().get(0).getProperties().getTitle(),(WithSheet withSheet)-> {

                    withSheet.writeCell("A2", "Hola Caracola"); //<1>
                    String holaCaracola = withSheet.readCell("A2").get(0).toString();   //<2>
                    assert holaCaracola.equals("Hola Caracola");

                    withSheet.writeInRange("A2","B4", (WriteInRange writeInRange)->{//<3>
                        writeInRange.addRow(Arrays.asList("Hola",null));
                        writeInRange.addRow(Arrays.asList("Hola","Caracola"));
                        writeInRange.addRow(Arrays.asList("Hola","=A2"));
                    });

                    List<List> range = withSheet.readRows("A2","B4");//<4>
                    assert range.size() == 3;
                    assert range.get(0).size()==1;
                    assert range.get(1).size()==2;
                    assert range.get(2).get(1).equals(range.get(0).get(0));
                });
                //end::java[]
            });
            //end::withSpreadSheet[]

            //tag::deleteSheet[]
            DriveScript.instance.withFile(sheetId, (WithFile withFile)->{
                withFile.removeFromDrive();
            });
            //end::deleteSheet[]

            DriveScript.instance.findFile(sheetId);
        }catch( Exception e){
            if( !(e instanceof GoogleJsonResponseException) )
                throw e;
            GoogleJsonResponseException ge = (GoogleJsonResponseException)e;
            assert ge.getStatusCode() == 404;
        }
    }


}
