package com.puravida.groogle.test;

import com.puravida.groogle.*;
import org.junit.Before;
import org.junit.Test;
import com.google.api.services.drive.model.File;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


public class ListSheetsTest {

    @Before
    public void doLogin(){
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-example");
        groogleScript.login(clientSecret, Arrays.asList("https://www.googleapis.com/auth/drive"));

        // Must to initilize DriveScript
        SheetScript.instance.setGroogleScript(groogleScript);
        DriveScript.instance.setGroogleScript(groogleScript);
        SheetScript.instance.setDriveScript(DriveScript.instance);
    }

    @Test
    public void allFiles() throws IOException{
        //tag::test[]
        for( File f : SheetScript.instance.getListSpreadSheets()){
            System.out.println(f.getId()+" "+f.getName()+" "+f.getParents());
        };
        //end::test[]
    }


}
