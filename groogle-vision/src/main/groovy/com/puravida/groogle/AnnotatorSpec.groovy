package com.puravida.groogle

import com.google.api.gax.core.CredentialsProvider
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.vision.v1.AnnotateImageRequest
import com.google.cloud.vision.v1.Feature
import com.google.cloud.vision.v1.Feature.Type
import com.google.cloud.vision.v1.Image
import com.google.cloud.vision.v1.ImageAnnotatorClient
import com.google.cloud.vision.v1.ImageAnnotatorSettings
import com.google.protobuf.ByteString


class AnnotatorSpec {

    protected ImageAnnotatorClient imageAnnotatorClient
    void setCredentialsProvider(CredentialsProvider credentialsProvider){
        ImageAnnotatorSettings imageAnnotatorSettings =
                ImageAnnotatorSettings.newBuilder().setCredentialsProvider(credentialsProvider) build();
        imageAnnotatorClient =
                ImageAnnotatorClient.create(imageAnnotatorSettings);
    }

    protected List<AnnotateImageRequest> annotateImageRequests = []

    Feature.Type label = Feature.Type.LABEL_DETECTION
    Feature.Type face = Feature.Type.FACE_DETECTION
    Feature.Type text = Feature.Type.TEXT_DETECTION

    AnnotatorSpec image(String path, Feature.Type...features){
        File f = new File(path)
        if( f.exists() )
            return image(f,features)
        InputStream inputStream = this.getClass().getResourceAsStream(path)
        if(inputStream)
            return image(inputStream,features)
        return image(new URL(path),features)
    }

    AnnotatorSpec image(File path, Feature.Type...features){
        image(path.newInputStream(), features)
    }

    AnnotatorSpec image(URL path, Feature.Type...features){
        image(path.newInputStream(), features)
    }

    AnnotatorSpec image(InputStream path, Feature.Type...features){
        AnnotateImageRequest.Builder builder = new AnnotateImageRequest.Builder()
        builder.image = new Image.Builder(content: ByteString.copyFrom(path.bytes)).build()
        builder.addAllFeatures( features.collect{
            Feature.newBuilder().setType(it).build();
        })
        annotateImageRequests.add( builder.build() )
        this
    }


    void execute(){
        println imageAnnotatorClient.batchAnnotateImages(annotateImageRequests).responsesList
    }

}
