package com.puravida.groogle

import com.google.api.gax.core.CredentialsProvider
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.vision.v1.AnnotateImageRequest
import com.google.cloud.vision.v1.Feature
import com.google.cloud.vision.v1.Feature.Type
import com.google.cloud.vision.v1.Image
import com.google.cloud.vision.v1.ImageAnnotatorClient
import com.google.cloud.vision.v1.ImageAnnotatorSettings
import com.google.protobuf.ByteString

@Singleton
class VisionScript implements LoginCloudTrait{

    CredentialsProvider credentialsProvider

    private VisionScript initScript( ) {
        if( credentialsProvider )
            return this
        credentialsProvider = FixedCredentialsProvider.create(ServiceAccountCredentials.fromStream(clientSecret))
        this
    }

    void parseImages( @DelegatesTo(strategy = Closure.DELEGATE_ONLY,value = AnnotatorSpec)Closure closure) {
        initScript()
        AnnotatorSpec delegate = new AnnotatorSpec(credentialsProvider:credentialsProvider)
        Closure clone = closure.rehydrate(delegate, delegate, delegate)
        clone()
        delegate.execute()
    }

}
